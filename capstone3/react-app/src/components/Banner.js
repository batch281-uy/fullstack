import { Button, Row, Col } from "react-bootstrap";

import { Link } from "react-router-dom";

export default function Banner() {
  return (
    <Row>
      <Col className="p-5">
        <h1>The General's Coffee</h1>
        <h2>Right away, sir!</h2>
        <Button as={Link} to="/courses" variant="primary">
          Purchase now!
        </Button>
      </Col>
    </Row>
  );
}
