import { Row, Col, Card } from "react-bootstrap";

export default function Highlights() {
  return (
    <Row className="mt-3 mb-3">
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Attention!</h2>
            </Card.Title>
            <Card.Text>
              The general's coffee philosophy is to ensure the quality of the
              coffee beans we deliver to you are fresh from the grinder. No time
              wasted. Attention to details from packing to delivery to ensure
              freshly roasted coffee is delivered at your doorsteps.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Patriotism and Loyalty!</h2>
            </Card.Title>
            <Card.Text>
              We support our local farmers. We love our farmers and we support
              them every way we can. In return, our farmers have been nothing
              but diligent and able to produce the best coffee for the world to
              taste!
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Be Part of Our Community</h2>
            </Card.Title>
            <Card.Text>
              Join us as we help bridge the gaps between our local farmers and
              the rest of the world. Let the world know the best of the
              Philippines. Mabuhay!
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
