import React, { useEffect, useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import Swal2 from "sweetalert2";

const ArchiveProduct = () => {
  const history = useHistory();
  const { productId } = useParams();
  const [product, setProduct] = useState(null);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((response) => response.json())
      .then((data) => setProduct(data));
  }, [productId]);

  const handleArchiveProduct = () => {
    if (product) {
      fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          isActive: false
        })
      })
        .then((response) => response.json())
        .then((data) => {
          if (data.success) {
            Swal2.fire({
              title: "Product Archived",
              icon: "success",
              text: "Product has been archived successfully."
            });
            history.push("/adminDashboard"); // Redirect to the admin dashboard after successful product archiving
          } else {
            Swal2.fire({
              title: "Error",
              icon: "error",
              text: "Failed to archive the product. Please try again."
            });
          }
        });
    }
  };

  if (!product) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h2>Archive Product</h2>
      <h3>{product.name}</h3>
      <p>{product.description}</p>
      <p>Price: ${product.price}</p>
      <p>Stock: {product.stock}</p>
      <button onClick={handleArchiveProduct}>Archive Product</button>
    </div>
  );
};

export default ArchiveProduct;
    }
    