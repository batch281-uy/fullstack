import React, { useState } from "react";
import { useHistory } from "react-router-dom";

const AddProduct = () => {
  const history = useHistory();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [stock, setStock] = useState(0);
  const [isActive, setIsActive] = useState(true);

  const handleAddProduct = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        stock: stock,
        isActive: isActive
      })
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          Swal2.fire({
            title: "Product Added",
            icon: "success",
            text: "Product has been successfully added."
          });
          history.push("/adminDashboard"); // Redirect to the admin dashboard after successful product addition
        } else {
          Swal2.fire({
            title: "Error",
            icon: "error",
            text: "Failed to add the product. Please try again."
          });
        }
      });
  };

  return (
    <div>
      <h2>Add Product</h2>
      <form onSubmit={handleAddProduct}>
        <label htmlFor="name">Name:</label>
        <input
          type="text"
          id="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          required
        />

        <label htmlFor="description">Description:</label>
        <textarea
          id="description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          required
        ></textarea>

        <label htmlFor="price">Price:</label>
        <input
          type="number"
          id="price"
          value={price}
          onChange={(e) => setPrice(e.target.value)}
          required
        />

        <label htmlFor="stock">Stock:</label>
        <input
          type="number"
          id="stock"
          value={stock}
          onChange={(e) => setStock(e.target.value)}
          required
        />

        <label htmlFor="isActive">Active:</label>
        <input
          type="checkbox"
          id="isActive"
          checked={isActive}
          onChange={(e) => setIsActive(e.target.checked)}
        />

        <button type="submit">Add Product</button>
      </form>
    </div>
  );
};

export default AddProduct;
