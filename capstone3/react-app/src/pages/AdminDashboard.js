import { Button, Col, Row, Container, Table } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate, Link } from "react-router-dom";

function AdminDashboard() {
  // const { name, brand, description, price, productType, stock } = productProp;
  const { user } = useContext(UserContext);

  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setProducts(data);
      });
  }, []);

  return user.isAdmin !== true ? (
    <Navigate to="*" />
  ) : (
    <div className="p-4">
      <Row className="justify-content-center text-center">
        <h2>Admin Dashboard</h2>
        <div>
          <Button as={Link} to={`/addproduct`} variant="primary">
            Add New Product
          </Button>
          {/* <Button variant="success">Show User Orders</Button> */}
        </div>
        <Table striped bordered hover className="mt-2 ">
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Price</th>

              <th>Stock</th>
              <th>Active</th>
            </tr>
          </thead>
          <tbody>
            {products.map((product) => {
              return (
                <tr key={product._id}>
                  <td>{product.name}</td>

                  <td>{product.description}</td>
                  <td>PHP {product.price}</td>

                  <td>{product.stock} stock(s)</td>
                  <td>{String(product.isActive)}</td>
                  <td>
                    {product.isActive ? (
                      <Button
                        as={Link}
                        to={`/archive/${product._id}`}
                        variant="outline-danger"
                      >
                        Archive
                      </Button>
                    ) : (
                      <Button
                        as={Link}
                        to={`/unarchive/${product._id}`}
                        variant="outline-success"
                      >
                        Unarchive
                      </Button>
                    )}

                    <Button
                      as={Link}
                      to={`/updateProducts/${product._id}`}
                      variant="outline-primary"
                    >
                      Update
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Row>
    </div>
  );
}

export default AdminDashboard;



import { useState, useEffect, useContext } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import { useParams } from "react-router-dom";
import CourseCard from "../components/CourseCard";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function AdminPortal() {
  const [show, setShow] = useState(false);
  const handleShow = () => {
  setShow(true);
};

// Define modal close functions
const handleClose = () => {
  setShow(false);
};
  
  return (
    <>
              <Button variant="danger" onClick={handleShow}>
                Delete
              </Button>
           
        <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add/Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product name"
                required

                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>
           
          </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Add Product
          </Button>
        </Modal.Footer>
      </Modal>

    </>
  );
}



return (
  <>
    <Card className="CourseCard p-3 mb-3 w-50">
      <Card.Img variant="top" src="{image}" alt="{name}" />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price</Card.Subtitle>
        <Card.Text>`₱ ${price}`</Card.Text>
      </Card.Body>

      {isActive ? (
        <>
          <Button variant="primary" className="w-50 mb-2">
            Edit
          </Button>
          <Button
            variant="danger"
            className="w-50 mb-2"
            onClick={deleteProduct}
          >
            Delete
          </Button>
          <Button
            variant="primary"
            className="w-50 mb-2"
            onClick={archiveProduct}
          >
            Archive
          </Button>
        </>
      ) : (
        <>
          <Button
            variant="primary"
            className="w-50 mb-2"
            onClick={handleShow}
          >
            Edit
          </Button>
          <Button variant="danger" className="w-50 mb-2">
            Delete
          </Button>
          <Button
            variant="secondary"
            className="w-50 mb-2"
            onClick={activateProduct}
          >
            Set as active
          </Button>
        </>
      )}
    </Card>

    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Add/Edit Product</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
            <Form.Label>Product Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter product name"
              required
              defaultValue={newName}
              onChange={(e) => setName1(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
            <Form.Label>Price</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter Price"
              required
              defaultValue={newPrice}
              onChange={(e) => setPrice1(e.target.value)}
            />
          </Form.Group>
          <Form.Group
            className="mb-3"
            controlId="exampleForm.ControlTextarea1"
          >
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              type="number"
              placeholder="Description"
              required
              defaultValue={newDescription}
              onChange={(e) => setDescription1(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="exampleForm.ControlInput3">
            <Form.Label>Add Product image link here</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              type="text"
              placeholder="Product link"
              required
              defaultValue={newImage}
              onChange={(e) => setNewImage(e.target.value)}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button variant="primary" onClick={updateProduct}>
          Update Product
        </Button>
      </Modal.Footer>
    </Modal>
  </>
);



import { useState, useEffect, useContext, Fragment } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import { useParams } from "react-router-dom";
import CourseCard from "../components/CourseCard";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import AdminCard from "../components/AdminCard";
export default function AdminPortal() {
  // Check to see if the mock data was captured
  // console.log(coursesData);

  const [products, setProducts] = useState([]);
  //we are goint to add an useEffect here so that in every time that we refresh our application it will fetch the updated content of our courses.

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/`)
      .then((response) => response.json())
      .then((data) => {
        // console.log(data);
        setProducts(
          data.map((product) => {
            // console.log(course);
            return <AdminCard key={product.id} productProp={product} />;
          })
        );
      });
  }, []);

  // The "map" method loops through the individual course objects in our array and returns a component for each course
  // Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed
  // Every time the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our courseData array using the courseProp
  // const courses = coursesData.map(course => {
  // 	return (
  // 		<CourseCard key = {course.id} courseProp={course} />
  // 	)
  // })

  // The course in the CourseCard component is called a prop which is a shorthand for "property" since components are considered as objects in ReactJS
  // The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions rather than hard coded values which use double quotes ""
  // We can pass information from one component to another using props. This is referred to as props drilling
  return <Fragment>{AdminCard}</Fragment>;
}
