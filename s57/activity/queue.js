let collection = [];

// Write the queue functions below.

function print() {
  // It will show the array
  // .length is allowed
  return collection;
}

function enqueue(element) {
  //In this function you are going to make an algo that will add an element to the array (last element)
  // .length is allowed
  collection[collection.length] = element;
  return collection;
}

function dequeue() {
  // In here you are going to remove the first element in the array
  // remove first element in the array
  // .length is allowed
  collection[0] = collection[1];
  collection.length = collection.length - 1;
  return collection;
}

function front() {
  // you will get the first element
  // get the first element
  // .length is allowed
  return collection[0];
}

function size() {
  // Number of elements
  // do not use .length
  let counter = 0;
  let i = 0;

  while (collection[i] !== null && collection[i] !== undefined) {
    counter++;
    i++;
  }
  return counter;
}

function isEmpty() {
  //it will check whether the function is empty or not
  // do not use .length
  let counter = 0;
  let i = 0;

  while (collection[i] !== null && collection[i] !== undefined) {
    counter++;
    i++;
  }
  return false;
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty
};
